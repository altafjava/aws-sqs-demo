package com.altafjava.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SQSController {

	@Autowired
	private QueueMessagingTemplate queueMessagingTemplate;

	@Value("${cloud.aws.endpoint.uri}")
	private String sqsDestnationUri;

	@GetMapping("/send-message")
	public String sendMessage() {
		queueMessagingTemplate.send(sqsDestnationUri, MessageBuilder.withPayload("hello from altafjava").build());
		return "message send successfully!";
	}

	@SqsListener("my-first-queue")
	public String recceiveMessage(String message) {
		System.out.println(message);
		return message;
	}
}
