package com.altafjava.controller;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.altafjava.model.Product;
import com.altafjava.repository.ProductRepository;

@RestController
public class DynamoDBController {

	@Autowired(required = true)
	private ProductRepository productRepository;

	@GetMapping("/items")
	public Iterable<Product> getAllItems() {
		Iterable<Product> products = productRepository.findAll();
		return products;
	}
}
