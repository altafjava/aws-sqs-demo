package com.altafjava.repository;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.altafjava.model.Product;

@EnableScan
@Repository
public interface ProductRepository extends CrudRepository<Product, String> {

}
