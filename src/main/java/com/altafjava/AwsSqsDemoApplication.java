package com.altafjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.altafjava.repository")
public class AwsSqsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsSqsDemoApplication.class, args);
		System.out.println("-------------------- APPLICATION STARTED -----------------------");
	}

}
